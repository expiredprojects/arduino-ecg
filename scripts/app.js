﻿
var lines;
var PreviewIndex = 0;
var RocknCoder = RocknCoder || {};
RocknCoder.Pages = RocknCoder.Pages || {};

RocknCoder.Pages.Kernel = function (event) {
	var that = this,
		eventType = event.type,
		pageName = $(this).attr("data-rockncoder-jspage");
	if (RocknCoder && RocknCoder.Pages && pageName && RocknCoder.Pages[pageName] && RocknCoder.Pages[pageName][eventType]) {
		RocknCoder.Pages[pageName][eventType].call(that);
	}
};

RocknCoder.Pages.Events = function () {
	$("div[data-rockncoder-jspage]").on(
		'pagebeforecreate pagecreate pagebeforeload pagebeforeshow pageshow pagebeforechange pagechange pagebeforehide pagehide pageinit',
		RocknCoder.Pages.Kernel).on(
		"pageinit", RocknCoder.hideAddressBar);
} ();

RocknCoder.Pages.managePlotChart = function () {
	var pageshow = function () {
//            $.get('log.txt', function(data) {
//                lines = data.split("\n");
//            });
            $.ajax({
                url: "wojtek.php?function=curl",
                type: "GET",
                async: false,
                success: function(data) {
                    lines = data.split("\n");
                }
            });
			setTimeout(function() {
                updateChart();
			    $("#refreshPlotChart").click(function(){
				updateChart();
				$.mobile.silentScroll();
			     });
            }, 1000);
		},
		pagehide = function () {
			$("#refreshPlotChart").unbind('click');
		},
		updateChart = function(){
            var len = lines.length;
            var vals = [];
            for (var i = PreviewIndex; i < PreviewIndex + 120; i++) {
                vals.push([i, parseInt(lines[i])]);
            }
            PreviewIndex += 30;
            if (PreviewIndex > len - 60) PreviewIndex = 0;
            showChart(vals);
		},
		showChart = function(vals){
            var options = {seriesDefaults: {showMarker: false}, axes: {yaxis: {min: 0, max: 800}}};
            var plot = $.jqplot('plotChart',[vals],options);
			plot.replot({clear: true, resetAxes: false});
		};
	return {
		pageshow: pageshow,
		pagehide: pagehide
	}
}();





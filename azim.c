#include <SPI.h>
#include <Ethernet.h>
byte mac[] = { 
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };
IPAddress ip(192,168,1,105);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255, 255, 255, 255);
EthernetServer server(80);
// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
  Serial.println("ready");
}
// the loop routine runs over and over again forever:
void loop() {
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {
      if (client.available()) {
        
        Serial.println();
				client.println("HTTP/1.1 200 OK");
				client.println("Content-Type:text/html");
				client.println();
	for (int i = 0; i < 1000; i++) {
            int sensorValue = analogRead(A0);
            client.println(sensorValue);
            client.println("<br />");
            delay(10);
        }
        break;
      }
    }
  delay(1);
  client.stop();  
  }
}